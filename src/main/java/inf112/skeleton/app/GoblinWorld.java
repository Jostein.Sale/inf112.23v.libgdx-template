package inf112.skeleton.app;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.Input.Keys;

public class GoblinWorld {
   // Hitboxes, vectors, speed and health
   private Rectangle player;
   private Rectangle attackBox;
   private Rectangle enemy;
   private Vector2 playerVec;
   private Vector2 enemeyVec;
   private float enemeyStopDistance = 100;
   private int playerSpeed = 5;
   private int enemeySpeed = 2;
   private int enemyHP = 50;
   private int attackDamage = 10;
   private boolean enemyIsDead = false;

   // Directions
   private String[] playerDirections = {"UP", "RIGHT", "DOWN", "LEFT"};
   private final int UP = 0;
   private final int RIGHT = 1;
   private final int DOWN = 2;
   private final int LEFT = 3;
   private int playerDir = RIGHT;   // Starting position

   // Actions
   private String[] playerActions = {"STANDING", "RUNNING", "ATTACK"};
   private final int STANDING = 0;
   private final int RUNNING = 1;
   private final int ATTACK = 2;
   private int playerAction = 0;
   private int attackCoolDown = 0;

   public GoblinWorld() {
      this.player = new Rectangle(
         Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2, 50, 50);
      this.playerVec = new Vector2(player.x, player.y);
      this.attackBox = new Rectangle(
         Gdx.graphics.getWidth()/2 + 50, Gdx.graphics.getHeight()/2, 50, 50);
      
      this.enemy = new Rectangle(
         200, 200, 50, 50);
      this.enemeyVec = new Vector2(0, 0);
   }

   public void update() {
      updatePlayer();
      if (!enemyIsDead) {
         updateEnemey();
      }
   }

   private void updateEnemey() {
      // Update vector
      enemeyVec = new Vector2(enemy.x + enemy.width / 2, enemy.y + enemy.height / 2);

      // Check if attacked.
      if ((attackCoolDown == 10) && (enemy.overlaps(attackBox))) {
         enemyHP -= attackDamage;
         if (enemyHP <= 0) {
            enemyIsDead = true;
         }
      }

      // Check if near player
      if (isPlayerNear(enemeyStopDistance)) {
         return;
      }
      // Calculate the vector from the enemy to the player
      Vector2 directon = playerVec.cpy().sub(enemeyVec).nor();

      // Update the enemy's position based on the direction and speed
      enemy.x += directon.x * enemeySpeed; //* Gdx.graphics.getDeltaTime();
      enemy.y += directon.y * enemeySpeed; //* Gdx.graphics.getDeltaTime();
      
   }

   private boolean isPlayerNear(float distanceThreshold) {
      // Calculate the distance between enemyCenter and playerCenter
      float distance = Math.abs(playerVec.dst(enemeyVec));
  
      // Check if the distance is smaller than the given threshold
      return distance < distanceThreshold;
  }

   private void updatePlayer() {
      playerVec = new Vector2(player.x + player.width / 2, player.y + player.height / 2);

      if (playerAction == ATTACK) {  // Player cannot move during attack-cooldown.
         attackCoolDown--;
         if (attackCoolDown == 0) {
            playerAction = STANDING;
         }
         return;
      }
      playerAction = STANDING;   // default action

      if (Gdx.input.isKeyPressed(Keys.SPACE)) {  // Attack
         playerAction = ATTACK;
         attackCoolDown = 10;
         updateAttackBox();
         return;
      }

      if (Gdx.input.isKeyPressed(Keys.LEFT)) {
         playerDir = LEFT;
         playerAction = RUNNING;
         if (player.x > 0) {
            player.x -= playerSpeed;
            if (!enemyIsDead && player.overlaps(enemy)) {
               player.x += playerSpeed;
            }
         }
      }
      if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
         playerDir = RIGHT;
         playerAction = RUNNING;
         if (player.x < (Gdx.graphics.getWidth() - player.width)) {
            player.x += playerSpeed;
            if (!enemyIsDead && player.overlaps(enemy)) {
               player.x -= playerSpeed;
            }
         }
      }
      if (Gdx.input.isKeyPressed(Keys.UP)) {
         playerDir = UP;
         playerAction = RUNNING;
         if (player.y < (Gdx.graphics.getHeight() - player.height)) {
            player.y += playerSpeed;
            if (!enemyIsDead && player.overlaps(enemy)) {
               player.y -= playerSpeed;
            }
         }
      }
      if (Gdx.input.isKeyPressed(Keys.DOWN)) {
         playerDir = DOWN;
         playerAction = RUNNING;
         if (player.y > 0) {
            player.y -= playerSpeed;
            if (!enemyIsDead && player.overlaps(enemy)) {
               player.y += playerSpeed;
            }
         }
      }
   }

   private void updateAttackBox() {
      if (playerDir == RIGHT) {
         attackBox.x = player.x + 50;
         attackBox.y = player.y;
      }
      else if (playerDir == UP) {
         attackBox.x = player.x;
         attackBox.y = player.y + 50;
      }
      else if (playerDir == DOWN) {
         attackBox.x = player.x;
         attackBox.y = player.y - 50;
      }
      else if (playerDir == LEFT) {
         attackBox.x = player.x - 50;
         attackBox.y = player.y;
      }
   }

   public void draw(SpriteBatch batch, ShapeRenderer shapeRenderer) {
      // Player
      shapeRenderer.setColor(Color.RED);
      shapeRenderer.rect(
         player.x, player.y, 
         player.width, player.height);
      if (playerAction == ATTACK) {
         shapeRenderer.rect(
            attackBox.x, attackBox.y, 
            attackBox.width, attackBox.height);
      }

      // Enemy
      if (!enemyIsDead) {
         shapeRenderer.setColor(Color.BLUE);
         shapeRenderer.rect(
         enemy.x, enemy.y, 
         enemy.width, enemy.height);
         shapeRenderer.line(enemeyVec, playerVec);
      }
      
      System.out.println(
         "Direction = " + playerDirections[playerDir] + "       Action = " + playerActions[playerAction]);
   }
}
