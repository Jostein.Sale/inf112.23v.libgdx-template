package inf112.skeleton.app;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import inf112.skeleton.app.BomberMan.Controller;
import inf112.skeleton.app.BomberMan.Model;
import inf112.skeleton.app.BomberMan.View;

public class MyBomberMan implements ApplicationListener {
   private SpriteBatch batch;
   private ShapeRenderer shapeRend;
   private BitmapFont font;
   private Model model;
   private View view;
   private Controller controller;

   @Override
   public void create() {
      batch = new SpriteBatch();
      shapeRend = new ShapeRenderer();
      font = new BitmapFont();
      font.setColor(Color.RED);
      controller = new Controller();
      model = new Model(controller);
      view = new View(model);
   }

   @Override
   public void dispose() {
      batch.dispose();
      font.dispose();
   }

   @Override
   public void render() {
      // Drawing
      Gdx.gl.glClearColor(1, 1, 1, 1);
      Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

      // Hitboxes
      shapeRend.begin(ShapeType.Filled);
      this.view.draw(shapeRend);
      shapeRend.end();

      // Images
      batch.begin();
      // TODO - make images
      batch.end();

      // Update
      controller.update();
      model.update();

   }

   @Override
   public void resize(int width, int height) {
   }

   @Override
   public void pause() {
   }

   @Override
   public void resume() {
   }
}
