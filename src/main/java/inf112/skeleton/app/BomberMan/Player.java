package inf112.skeleton.app.BomberMan;

public class Player {
   private int row;
   private int col;
   private int moveTimer = 0;
   private int speed = Constants.PLAYER_SPEED;

   public Player(int row, int col) {
      this.row = row;
      this.col = col;
   }

   public void updateWait() {
      if (moveTimer > 0) {
         moveTimer--;
      } 
   }

   public boolean canMove() {
      return moveTimer == 0;
   }

   public void startMoveTimer() {
      this.moveTimer = speed;
   }

   public void setRow(int row) {
      this.row = row;
   }

   public void setCol(int col) {
      this.col = col;
   }

   public int getRow() {
      return row;
   }

   public int getCol() {
      return col;
   }

   
}
