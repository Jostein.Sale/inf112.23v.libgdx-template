package inf112.skeleton.app.BomberMan;

public record GridCell(int row, int col, char content) {}
