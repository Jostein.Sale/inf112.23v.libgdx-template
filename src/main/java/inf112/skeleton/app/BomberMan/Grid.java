package inf112.skeleton.app.BomberMan;

import java.util.ArrayList;

public class Grid {
   int rows;
   int cols; 
   ArrayList<ArrayList<GridCell>> grid;

   public Grid(int rows, int cols) {
      // Vi kan begynne med å sjekke at rows og cols har riktig størrelse -> hvis ikke: throw exception.
      this.rows = rows;
      this.cols = cols;
      this.grid = new ArrayList<>();
      constructGrid();
      //printGrid();   // Brukes for testing
   }

   private void constructGrid() {
      for (int r = 0; r < rows; r++) {
         grid.add(new ArrayList<>());
         for (int c = 0; c < cols; c++) {
            char content;
            // We're maybe going to have a hard-coded map instead. But this is fine for now.
            if (c % 2 == 0) {content = Constants.WALL;} else {content = Constants.EMPTY;};
            if (r == 0 || r == (rows - 1)) {content = Constants.WALL;}
            if (r % 2 == 1) {content = Constants.EMPTY;}
            if (c == 0 || c == (cols -1 )) {content = Constants.WALL;};
            grid.get(r).add(new GridCell(r, c, content));
         }
      }
      
   }

   private void printGrid() {
      for (int r = 0; r < rows; r++) {
         for (int c = 0; c < cols; c++) {
            System.out.print(grid.get(r).get(c).content());
         }
         System.out.println();
      }
   }

   public char getContent(int r, int c) {
      return this.grid.get(r).get(c).content();
   }

   public boolean canMoveHere(int r, int c) {
      return grid.get(r).get(c).content() == Constants.EMPTY;
   }
}
