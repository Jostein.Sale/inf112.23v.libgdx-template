package inf112.skeleton.app.BomberMan;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class View {
   private Model model;

   public View(Model model) {
      this.model = model;
   }

   public void draw(ShapeRenderer shapeRend) {
      drawMap(shapeRend);
      drawPlayer(shapeRend);
      
   }

   private void drawPlayer(ShapeRenderer shapeRend) {
      shapeRend.setColor(Constants.PLAYER_COLOR);
      int row = model.getPlayerRow();
      int col = model.getPlayerCol();
      int xPos = Constants.WIDTH_MARGIN + Constants.CELL_SIZE * col;
      int yPos = Constants.HEIGHT_MARGIN + Constants.CELL_SIZE * row;
      shapeRend.rect(
         xPos, yPos,
         Constants.CELL_SIZE, Constants.CELL_SIZE
      );
   }

   private void drawMap(ShapeRenderer shapeRend) {
      for (int r = 0; r < Constants.ROWS; r++) {
         for (int c = 0; c < Constants.COLS; c++) {
            char content = model.getContent(r, c);
            setColor(shapeRend, content);
            shapeRend.rect(
               (Constants.WIDTH_MARGIN + c * Constants.CELL_SIZE),   // x coordinate
               (Constants.HEIGHT_MARGIN + r * Constants.CELL_SIZE),  // y coordinate
               Constants.CELL_SIZE, Constants.CELL_SIZE              // width, height
            );
         }
      }
   }

   private void setColor(ShapeRenderer shapeRend, char content) {
      Color c = Constants.getColor(content);
      shapeRend.setColor(c);
   }
}
