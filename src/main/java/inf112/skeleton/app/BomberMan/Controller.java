package inf112.skeleton.app.BomberMan;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public class Controller {
   public boolean leftIsPressed = false;
   public boolean rightIsPressed = false;
   public boolean upIsPressed = false;
   public boolean downIsPressed = false;
   public boolean bombIsPressed = false;

   public void update() {
      resetKeys();
      if (Gdx.input.isKeyPressed(Keys.SPACE)) {  // Attack
         bombIsPressed = true;
      }
      if (Gdx.input.isKeyPressed(Keys.LEFT)) {
         leftIsPressed = true;
      }
      if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
         rightIsPressed = true;
      }
      if (Gdx.input.isKeyPressed(Keys.UP)) {
         upIsPressed = true;
      }
      if (Gdx.input.isKeyPressed(Keys.DOWN)) {
         downIsPressed = true;
      }
   }

   private void resetKeys() {
      leftIsPressed = false;
      rightIsPressed = false;
      upIsPressed = false;
      downIsPressed = false;
      bombIsPressed = false;
   }
}
