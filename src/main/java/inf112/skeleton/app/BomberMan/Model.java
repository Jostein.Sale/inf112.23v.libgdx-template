package inf112.skeleton.app.BomberMan;

public class Model {
   private Grid grid;
   private Player player;
   private Controller controller;

   public Model(Controller controller) {
      grid = new Grid(Constants.ROWS, Constants.COLS);
      this.player = new Player(Constants.PLAYER_START_ROW, Constants.PLAYER_START_COL);
      this.controller = controller;
   }

   public void update() {
      player.updateWait();
      if (player.canMove()) {
         checkPlayerMovement();
      }
   }

   private void checkPlayerMovement() {
      if (controller.rightIsPressed) {
         controller.rightIsPressed = false;
         player.startMoveTimer();
         if (grid.canMoveHere(player.getRow(), player.getCol() + 1)) {
            player.setCol(player.getCol() + 1);
         }
      }
      else if (controller.leftIsPressed) {
         controller.leftIsPressed = false;
         player.startMoveTimer();
         if (grid.canMoveHere(player.getRow(), player.getCol() - 1)) {
            player.setCol(player.getCol() - 1);
         }
      }
      else if (controller.upIsPressed) {
         controller.upIsPressed = false;
         player.startMoveTimer();
         if (grid.canMoveHere(player.getRow() + 1, player.getCol())) {
            player.setRow(player.getRow() + 1);
         }
      }
      else if (controller.downIsPressed) {
         controller.downIsPressed = false;
         player.startMoveTimer();
         if (grid.canMoveHere(player.getRow() - 1, player.getCol())) {
            player.setRow(player.getRow() - 1);
         }
      }
   }

   public char getContent(int r, int c) {
      return this.grid.getContent(r, c);
   }

   public int getPlayerCol() {
      return this.player.getCol();
   }

   public int getPlayerRow() {
      return this.player.getRow();
   }
}
