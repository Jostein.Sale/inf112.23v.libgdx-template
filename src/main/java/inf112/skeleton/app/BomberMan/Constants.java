package inf112.skeleton.app.BomberMan;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;

public class Constants {
   // Screen dimensions
   public static final int GAME_WIDTH = 1000;
   public static final int GAME_HEIGHT = 750;

   // Grid dimensions
   public static final int CELL_SIZE = 40;
   public static final int ROWS = 17;   // Should be an odd number
   public static final int COLS = 17;  // Should be an odd number
   public static final int WIDTH_MARGIN = (Gdx.graphics.getWidth() - CELL_SIZE * COLS) / 2;
   public static final int HEIGHT_MARGIN = (Gdx.graphics.getHeight() - CELL_SIZE * ROWS) / 2;

   // Cell content
   public static final char WALL = 't';
   public static final char EMPTY = '-';

   // Cell colors
   public static final Color WALL_COLOR = Color.BLACK;
   public static final Color EMPTY_COLOR = Color.WHITE;
   public static final Color PLAYER_COLOR = Color.BLUE;

   public static Color getColor(char content) {
      switch (content) {
         case WALL : return WALL_COLOR;
         case EMPTY : return EMPTY_COLOR;
         default : return Color.CYAN;
      }
   }

   // Player specs
   public static final int PLAYER_SHIELD = 10;
   public static final int PLAYER_SPEED = 7;
   public static final int PLAYER_START_ROW = 1;
   public static final int PLAYER_START_COL = 1;
   public static final int PLAYER_HP = 50;
}
