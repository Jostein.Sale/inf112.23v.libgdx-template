package inf112.skeleton.app;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

import inf112.skeleton.app.BomberMan.Constants;

public class Main {
    public static void main(String[] args) {
        
        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setTitle("BomberMan");
        cfg.setWindowedMode(Constants.GAME_WIDTH, Constants.GAME_HEIGHT);
        cfg.setForegroundFPS(60);

        new Lwjgl3Application(new MyBomberMan(), cfg);
    }
}