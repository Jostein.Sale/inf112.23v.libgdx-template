package inf112.skeleton.app;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class GoblinPuncher implements ApplicationListener {
    private SpriteBatch batch;
    private ShapeRenderer shapeRend;
    private BitmapFont font;
    private GoblinWorld gameWorld;

    @Override
    public void create() {
        batch = new SpriteBatch();
        shapeRend = new ShapeRenderer();
        font = new BitmapFont();
        font.setColor(Color.RED);
        gameWorld = new GoblinWorld();
    }

    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
    }

    @Override
    public void render() {
        // Drawing
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        // Hitboxes
        shapeRend.begin(ShapeType.Filled);
        gameWorld.draw(batch, shapeRend);
        shapeRend.end();

        // Images
        batch.begin();
        // TODO - make images
        batch.end();

        // Update
        gameWorld.update();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }
}
